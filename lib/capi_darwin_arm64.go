// Code generated by 'ccgo -export-defines  -export-enums  -export-externs X -export-fields F -export-structs  -export-typedefs  -o lib/xau_darwin_arm64.go -pkgname xau -trace-translation-units /var/folders/4f/mc8mts295pqf7gmnfcwh6g8w0000gn/T/go-generate-2901277256/cdb.json libXau.a', DO NOT EDIT.

package xau

var CAPI = map[string]struct{}{
	"XauDisposeAuth":       {},
	"XauFileName":          {},
	"XauGetAuthByAddr":     {},
	"XauGetBestAuthByAddr": {},
	"XauLockAuth":          {},
	"XauReadAuth":          {},
	"XauUnlockAuth":        {},
	"XauWriteAuth":         {},
}
