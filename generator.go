// Copyright 2021 The Xau-Go Authors. All rights reserved.
// Use of the source code is governed by a BSD-style
// license that can be found in the LICENSE file.

//go:build ignore
// +build ignore

package main

import (
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"

	"modernc.org/ccgo/v3/lib"
)

const (
	tarFile = tarName + ".tar.gz"
	tarName = "libXau-1.0.9"
)

type supportedKey = struct{ os, arch string }

var (
	gcc       = ccgo.Env("GO_GENERATE_CC", ccgo.Env("CC", "gcc"))
	gxx       = ccgo.Env("GO_GENERATE_CXX", ccgo.Env("CXX", "g++"))
	goarch    = ccgo.Env("TARGET_GOARCH", runtime.GOARCH)
	goos      = ccgo.Env("TARGET_GOOS", runtime.GOOS)
	supported = map[supportedKey]struct{}{
		{"darwin", "amd64"}: {},
		{"darwin", "arm64"}: {},
		{"linux", "386"}:    {},
		{"linux", "amd64"}:  {},
		{"linux", "arm"}:    {},
		{"linux", "arm64"}:  {},
	}
	tmpDir           = ccgo.Env("GO_GENERATE_TMPDIR", "")
	verboseCompiledb = ccgo.Env("GO_GENERATE_VERBOSE", "") == "1"
)

func main() {
	fmt.Printf("Running on %s/%s.\n", runtime.GOOS, runtime.GOARCH)
	if _, ok := supported[supportedKey{goos, goarch}]; !ok {
		ccgo.Fatalf(true, "unsupported target: %s/%s", goos, goarch)
	}

	ccgo.MustMkdirs(true,
		"lib",
		"internal/autest",
	)
	if tmpDir == "" {
		tmpDir = ccgo.MustTempDir(true, "", "go-generate-")
		defer os.RemoveAll(tmpDir)
	}
	ccgo.MustUntarFile(true, filepath.Join(tmpDir), tarFile, nil)
	cdb, err := filepath.Abs(filepath.Join(tmpDir, "cdb.json"))
	if err != nil {
		ccgo.Fatal(true, err)
	}

	cc, err := exec.LookPath(gcc)
	if err != nil {
		ccgo.Fatal(true, err)
	}

	cxx, err := exec.LookPath(gxx)
	if err != nil {
		ccgo.Fatal(true, err)
	}

	os.Setenv("CC", cc)
	os.Setenv("CXX", cxx)
	cfg := []string{
		"--enable-shared=no",
		"--disable-xthreads", //TODO
	}
	make := "make"
	switch goos {
	case "darwin":
		make = "gmake"
	}
	if _, err := os.Stat(cdb); err != nil {
		if !os.IsNotExist(err) {
			ccgo.Fatal(true, err)
		}

		ccgo.MustInDir(true, filepath.Join(tmpDir, tarName), func() error {
			ccgo.MustShell(true, "./configure", cfg...)
			switch {
			case verboseCompiledb:
				ccgo.MustRun(true, "-verbose-compiledb", "-compiledb", cdb, make, "check")
			default:
				ccgo.MustRun(true, "-compiledb", cdb, make, "check")
			}
			return nil
		})
	}
	ccgo.MustRun(true,
		"-export-defines", "",
		"-export-enums", "",
		"-export-externs", "X",
		"-export-fields", "F",
		"-export-structs", "",
		"-export-typedefs", "",
		"-o", filepath.Join("lib", fmt.Sprintf("xau_%s_%s.go", goos, goarch)),
		"-pkgname", "xau",
		"-trace-translation-units",
		cdb, "libXau.a",
	)
	ccgo.MustRun(true,
		"-export-fields", "F",
		"-lmodernc.org/xau/lib",
		"-o", filepath.Join("internal", "autest", fmt.Sprintf("main_%s_%s.go", goos, goarch)),
		"-trace-translation-units",
		cdb, "Autest",
	)
}
