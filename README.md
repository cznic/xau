# xau

CGo-free port of libXau, the authorization protocol for X.


## Installation

    $ go get modernc.org/xau

## Linking using ccgo

    $ ccgo foo.c -lmodernc.org/xau/lib

## Documentation

[godoc.org/modernc.org/xau](http://godoc.org/modernc.org/xau)

## Builders

[modern-c.appspot.com/-/builder/?importpath=modernc.org%2fxau](https://modern-c.appspot.com/-/builder/?importpath=modernc.org%2fxau)
